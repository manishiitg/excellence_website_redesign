$(document).ready(function(){

    $(document).on('click', '.nav.nav-tabs a[data-toggle="tab"]', function(e){
        e.preventDefault();
        $('.contact-forms .forms-container').removeClass('first-load');
        extraSpaceOnLoad = Number($(".nav-tabs li").first().offset().left - 1);
        index = ($(this).index());
        activeWidth = $(this).outerWidth();
        activePostion = $(this).offset().left - extraSpaceOnLoad;

        // Set Left Value;
        $(".nav-zoomout").css({
            "left": activePostion
        });

        // Set Width after Click;
        $(".nav-zoomout").css({
            width: activeWidth
        });
        
        var elemId = $(this).attr('href');
        $('.nav.nav-tabs a[data-toggle="tab"]').closest('li').removeClass('active');
        $(this).closest('li').addClass('active');
        $('.tab-content .tab-pane').removeClass('active').removeClass('in');
        $('.tab-content .tab-pane' + elemId).addClass('active').addClass('in');
    });
});

$(window).on('load', function(){
    console.log($('.inputBox').height()-15, $('.textAreaBox .form-control.textarea'));
    
    $('.textAreaBox .form-control.textarea').css('min-height', $('.inputBox').height()-15);
    setTimeout(function(){
        $('.textAreaBox .form-control.textarea').css('min-height', $('.inputBox').height()-15);
    }, 500);
});
