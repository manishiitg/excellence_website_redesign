// services
$(document).on('click', '.service-nav-menu li', function () {
    $('.service-nav-menu li').not(this).removeClass('active');
    $(this).addClass('active');

    $('.service-content div').removeClass('active');
    $($(this).data("target")).addClass('active');
});
// services


// inside-work-projects
$(document).on('click', '.projects .pagination .bullets', function () {
    $('.projects .pagination .bullets').not(this).removeClass('active');
    $(this).addClass('active');

});
jQuery(document).ready(function ($) {
    var slideWidth = 0;
    var index = 0
    var slideCount = 0;
    setTimeout(function () {
        slideCount = $('.projects .slider-container .slide').length;
        slideWidth = $('.projects .slider-container .slide').width();
        var slideHeight = $('.projects .slider-container .slide').height();
        var sliderUlWidth = slideCount * slideWidth;
        console.log(slideCount, 'slideCount')
        console.log(slideWidth, 'slideWidth')
        console.log(slideHeight, 'slideHeight')
        console.log(sliderUlWidth, 'sliderUlWidth')
    }, 100)
    $(document).on('click', '.projects .pagination .bullets', function () {
        $('.projects .pagination .bullets').not(this).removeClass('active');
        $(this).addClass('active');

        index = $('.projects .pagination .bullets').index(this);
        if (index) {
            $('.projects .slider-container .slides').css({ transform: `translateX(-${slideWidth * index}px)`, transition: 'ease-out .5s' });
        } else {
            $('.projects .slider-container .slides').css({ transform: `translateX(0px)`, transition: 'ease-out .5s' });
        }
    });
    $(document).on('click', '.projects .pagination img', function () {
        console.log('hi', $('.projects .pagination img').index(this))
        if ($('.projects .pagination img').index(this)) { // right click
            if ((index + 1) == slideCount) {
                index = 0;
                $('.projects .slider-container .slides').css({ transform: `translateX(0px)`, transition: 'ease-out .5s' });
            } else {
                ++index;
                $('.projects .slider-container .slides').css({ transform: `translateX(-${slideWidth * index}px)`, transition: 'ease-out .5s' });
            }
        } else { // left click
            if (index) {
                --index;
                $('.projects .slider-container .slides').css({ transform: `translateX(-${slideWidth * index}px)`, transition: 'ease-out .5s' });
            } else {
                index = slideCount - 1;
                $('.projects .slider-container .slides').css({ transform: `translateX(-${slideWidth * index}px)`, transition: 'ease-out .5s' });
            }
        }
        $('.projects .pagination .bullets').eq(index).addClass('active').siblings().removeClass('active');
    });


});

// inside-work-projects
