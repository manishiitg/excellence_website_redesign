$(window).ready(function(){
    $(document).on('click', '.clients .testimonials .client-slider div', function(ev){

        // IMAGES SLIDE
        const a = this.classList[0] == 1 ? 3 : this.classList[0] == 2 ? 1 : 2;
        const b = this.classList[0] == 1 ? 1 : this.classList[0] == 2 ? 2 : 3;
        const c = this.classList[0] == 1 ? 2 : this.classList[0] == 2 ? 3 : 1;
        $(`.${a}`).removeClass('left').removeClass('center').removeClass('right').addClass('left');
        $(`.${b}`).removeClass('left').removeClass('center').removeClass('right').addClass('center');
        $(`.${c}`).removeClass('left').removeClass('center').removeClass('right').addClass('right');

        // TEXTS SLIDE
        $(`.testimonials-comments > div`).hide();
        $(`.testimonials-comments > div:nth-child(${this.classList[0]})`).show();

        // BULLETS
        $(`.testimonials .bullets i`).removeClass('active');
        $(`.testimonials .bullets i:nth-child(${this.classList[0]})`).addClass('active');
    });
});