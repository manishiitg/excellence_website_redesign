$(document).on('click', '.service-nav-menu li', function () {
    $('.service-nav-menu li').not(this).removeClass('active');
    $(this).addClass('active');

    $('.service-content div').removeClass('active');
    $($(this).data("target")).addClass('active');
});

$(document).on('click', '.menu-button', function(){
    $('.menu-parent, .menu, .menu-button').toggleClass('open');
});

/* CLOSE MENU ON OUTSIDE MENU DIV AREA  */
if (document.addEventListener) { // W3C DOM
    document.addEventListener('click',mouseClickOutsieMenu,false);
} else if (document.attachEvent) { // IE DOM
    document.attachEvent('onclick', mouseClickOutsieMenu);
} else { // No much to do
    document["on"+'click'] = mouseClickOutsieMenu;
}
function mouseClickOutsieMenu(e) {
    var target = e.target || e.srcElement; 
    var classes = target.className || target.parentNode.className || target.parentNode.parentNode.className;
    if( classes !== 'menu-parent' &&
        classes !== 'menu' &&
        classes !== 'menu open' &&
        classes !== 'sidebar' &&
        classes !== 'menu-button open' &&
        classes !== 'excellence-logo' &&
        classes !== 'chat' &&
        classes !== 'menu-items' &&
        classes !== 'social' &&
        classes !== 'linkedin' &&
        classes !== 'fb' &&
        classes !== 'insta' &&
        classes !== 'whatsapp') {
            if($('.menu-parent').hasClass('open')){
                $('.menu-parent, .menu, .menu .sidebar .menu-button').removeClass('open');
            }
        }
};

/* CLOSE MENU ON HITTING Esc BUTTON  */

$(window).keyup(function(e) {
    if(e.keyCode === 27 && $('.menu-parent').hasClass('open')) {
        $('.menu-parent, .menu, .menu .sidebar .menu-button, .menu-parent').removeClass('open');
    }
});